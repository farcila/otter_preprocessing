
### Changes on `otter_preprocessing_v3.py` for upgrading to Otter v 3.2.0


- `-j` flag removed in [#292](https://github.com/ucbds-infra/otter-grader/pull/292/)
- script command line added
- change `grades['total'] = grades.loc[:, grades.columns != 'identifier'].sum(axis=1)`
    -  ```bash
        FutureWarning: Dropping of nuisance columns in DataFrame reductions (with 'numeric_only=None') is deprecated;
        ```
- remove `create_json(ids, TARGET_PATH)`
- use 'identifier' as key to merge the grades and templates tables
