"""
AUTHOR: Jonas Scharfenberger <scharfen@leuphana.de>
This Script:

1. "move_submissions" move files after download to our grading folder
    and creates submissions/ipynbs directory
    pre: Submissions located in DOWNLOAD_PATH/ZIP_NAME
    post: Each submitted Notebook is located in TARGET_PATH/submissions/
    returns: dictionary (filename: moodle_id)

2. Create json file ("meta.json") that stores meta-data of the submissions
    which are needed for autograding
    input: dictionary (filename: moodle_id)

3. If wanted (GRADING = True) the "otter grade" command is executed.
    Note: Make sure that docker is running.
    => "final_grades.csv"  is generated and stored in TARGET_PATH directory
"""
import os
import json
import shutil
import pandas as pd
import argparse
from argparse import RawTextHelpFormatter


# define args
parser = argparse.ArgumentParser(
    description=__doc__, formatter_class=RawTextHelpFormatter
)
parser.add_argument(
    "-z",
    "--zip_file",
    help="File name of the zip file containing the student's submissions",
)
parser.add_argument(
    "-d",
    "--download_path",
    help="Path to the folder containing the downloaded zip file",
)
parser.add_argument(
    "-p",
    "--target_path",
    help="Path to the folder containing master notebook & 'dist' folder",
)
parser.add_argument(
    "-t",
    "--template",
    help="Name of the downloaded moodle grading .CSV template - assumed to be in DOWNLOAD_PATH",
)
parser.add_argument(
    "-grade",
    action="store_true",
    help="If the flag is set the 'otter grade' command is executed.",
)

args = parser.parse_args()
# get parameters and define relative paths
ZIP_NAME = args.zip_file
DOWNLOAD_PATH = os.path.normpath(os.path.join(os.getcwd(), args.download_path))
TARGET_PATH = os.path.normpath(os.path.join(os.getcwd(), args.target_path))
TEMPLATE_NAME = os.path.join(DOWNLOAD_PATH, args.template)
GRADING = args.grade


def move_submissions(target, dl_path, zip_file):

    if not os.path.exists(os.path.join(target, "submissions/ipynbs")):
        os.makedirs(os.path.join(target, "submissions/ipynbs"))

    target = os.path.join(target, "submissions/ipynbs")
    zip_path = os.path.join(dl_path, zip_file)
    extract_path = os.path.join(dl_path, zip_file[:-4])
    shutil.unpack_archive(zip_path, extract_path)
    helper = 1
    id_dict = {}
    upload_ids, stud_names, reason = [], [], []

    for folder in os.listdir(extract_path):
        print(folder, f"Moved {helper}/{len(os.listdir(extract_path))} files \r")
        folder_path = os.path.join(extract_path, folder)
        if "_" not in folder:
            continue
        stud_id = folder.split("_")[1]
        for file in os.listdir(folder_path):
            if file.endswith(".ipynb"):
                try:
                    data = open(os.path.join(folder_path, file), "r").read()

                    if '"cell_type"' in data:
                        path_to_nb = os.path.join(folder_path, file)
                        target_name = "submission" + stud_id + ".ipynb"
                        target_nb = os.path.join(target, target_name)
                        _ = shutil.move(path_to_nb, target_nb)
                        id_dict[target_name] = stud_id
                        break
                    else:
                        if not os.path.exists(
                            os.path.join(TARGET_PATH, "false_format")
                        ):
                            os.makedirs(os.path.join(TARGET_PATH, "false_format"))
                        path_to_nb = os.path.join(folder_path, file)
                        target_name = "submission" + stud_id + ".ipynb"
                        _ = shutil.move(
                            path_to_nb,
                            os.path.join(
                                os.path.join(TARGET_PATH, "false_format"), target_name
                            ),
                        )
                        upload_ids.append(stud_id)
                        stud_names.append(folder.split("_")[0])
                        reason.append("Richtiger Suffix, aber kein Notebook")
                except UnicodeDecodeError:

                    if not os.path.exists(os.path.join(TARGET_PATH, "false_format")):
                        os.makedirs(os.path.join(TARGET_PATH, "false_format"))
                    path_to_nb = os.path.join(folder_path, file)
                    target_name = "submission" + stud_id + ".ipynb"
                    _ = shutil.move(
                        path_to_nb,
                        os.path.join(
                            os.path.join(TARGET_PATH, "false_format"), target_name
                        ),
                    )
                    upload_ids.append(stud_id)
                    stud_names.append(folder.split("_")[0])
                    reason.append("Richtiger Suffix, aber falsche Formatierung")
            else:
                if not os.path.exists(os.path.join(TARGET_PATH, "false_format")):
                    os.makedirs(os.path.join(TARGET_PATH, "false_format"))
                path_to_nb = os.path.join(folder_path, file)
                target_name = "submission" + stud_id + ".ipynb"
                _ = shutil.move(
                    path_to_nb,
                    os.path.join(
                        os.path.join(TARGET_PATH, "false_format"), target_name
                    ),
                )
                upload_ids.append(stud_id)
                stud_names.append(folder.split("_")[0])
                reason.append("Falscher Suffix")

        helper += 1

    df = pd.DataFrame(
        list(zip(upload_ids, stud_names, reason)),
        columns=["Upload_ID", "Student_Name", "Problem"],
    )
    df.to_csv(os.path.join(TARGET_PATH, "not_graded.csv"), sep=";", index=False)

    return id_dict


def clean_notebook(cwd, cells=["1_CjJglt0UiT", "stv6YbjQ0Vlg", "Zbp9fd_s96_4"]):

    if not os.path.exists(os.path.join(cwd, "manual_grading")):
        os.makedirs(os.path.join(cwd, "manual_grading"))

    wd = os.path.join(cwd, "submissions/ipynbs")
    for file in os.listdir(wd):
        manual_grading = False
        if file.endswith("ipynb"):
            data = open(os.path.join(wd, file))
            notebook = json.load(
                data,
            )
            # Hopefully gets all solutions using infinite loops
            if "KeyboardInterrupt" in " ".join(
                open(os.path.join(wd, file)).readlines()
            ):
                shutil.move(
                    os.path.join(wd, file),
                    os.path.join(os.path.join(cwd, "manual_grading"), file),
                )
                continue

            for code_id in cells:

                for code_cell in notebook["cells"]:
                    try:
                        if code_cell["metadata"]["id"] == code_id:

                            if code_cell["source"] != [
                                line
                                for line in code_cell["source"]
                                if "input(" not in line
                            ]:
                                manual_grading = True
                                break
                    except:  # todo: specific exception E722
                        continue

            if manual_grading:
                shutil.move(
                    os.path.join(wd, file),
                    os.path.join(os.path.join(cwd, "manual_grading"), file),
                )


def find_suspicious(cwd):

    if not os.path.exists(os.path.join(cwd, "suspicious")):
        os.makedirs(os.path.join(cwd, "suspicious"))

    wd = os.path.join(cwd, "submissions/ipynbs")
    for file in os.listdir(wd):

        if file.endswith("ipynb"):
            # commented out to avoid F841
            # data = open(os.path.join(wd, file))
            # notebook = json.load(
            #     data,
            # )

            # Get submissions using gist
            if "view-in-github" in " ".join(open(os.path.join(wd, file)).readlines()):
                shutil.copyfile(
                    os.path.join(wd, file),
                    os.path.join(os.path.join(cwd, "suspicious"), file),
                )
                continue


def grade(target):

    os.chdir(target)

    ipynbs_path = "submissions/ipynbs"
    grader_path = "dist/autograder/autograder.zip"
    command = f"otter grade -p {ipynbs_path} -a {grader_path} -v"
    # rename autograder file
    rename_autog = "mv dist/autograder/*autograder*.zip dist/autograder/autograder.zip"
    os.system(rename_autog)
    os.system(command)


def enter_grades(target, download_path, template_name):

    path2grades = os.path.join(target, "final_grades.csv")
    path2temp = os.path.join(download_path, template_name)
    template = pd.read_csv(path2temp)
    grades = pd.read_csv(path2grades)

    # Ensure that ID column from template has the right name
    id_col_name = template.columns[0]
    # grade_col_name = template.columns[4]
    # Calculate sum of points of every student
    grades["identifier"] = grades["file"].str[-12:-6]
    template["identifier"] = template[id_col_name].str[-6:]
    # Added to avoid future warning pandas todo: define tasks column names as variables
    grades["total"] = grades[["task1", "task2", "task3"]].sum(axis=1)
    # Extract submission id to join both tables
    if template[id_col_name].dtype == "object":
        template[id_col_name] = template[id_col_name].str[-6:]
    # Assure they have the same data type
    template.identifier, grades.identifier = (
        template[id_col_name].astype(int),
        grades.identifier.astype(int),
    )
    # Merge both dataframe and store grades in the "Bewertung" column
    df = pd.merge(
        template, grades[["identifier", "total"]], how="left", on="identifier"
    )
    df.loc[df.total == df.total, "identifier"] = df.loc[df.total == df.total, "total"]
    df.loc[df.total != df.total, "identifier"] = 0
    # Drop helper columns
    df.drop(["identifier", "total"], axis=1, inplace=True)
    df["identifier"] = template["identifier"].astype(str)
    # Save the updated csv file
    output_path = os.path.join(target, template_name)
    df.to_csv(output_path, index=False, encoding="utf-8-sig")


def main():
    # ids = move_submissions removed F841
    move_submissions(target=TARGET_PATH, dl_path=DOWNLOAD_PATH, zip_file=ZIP_NAME)
    clean_notebook(TARGET_PATH)
    find_suspicious(TARGET_PATH)
    if GRADING:
        grade(TARGET_PATH)
        enter_grades(TARGET_PATH, DOWNLOAD_PATH, TEMPLATE_NAME)
