from setuptools import setup

setup(
    name="otter-preprocessing",
    version="0.1",
    description="Otter pre processing tool for DATAx",
    url="https://github.com/franasa/otter_preprocessing",
    author="",
    author_email="",
    license="GNU Affero General Public License v3.0",
    # packages=["otter_preprocessing"],
    entry_points={
        "console_scripts": [
            "otter_preprocessing=otter_preprocessing:main",
        ]
    },
    install_requires=["pandas==1.3.3", "otter-grader==3.2.0"],
    zip_safe=False,
)
