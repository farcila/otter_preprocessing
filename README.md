# `otter_preprocessing` for DATAx

## Requirements:
- Python3.8+
- Docker

## Installation:
- Clone this repository `git clone https://gitlab.gwdg.de/farcila/otter_preprocessing`
- Install the tool by running `pip install . --user` remove the `--user` flag if workin on an env

## Setup:
- Create a folder and unzip the file `Otter Notebooks.zip` containing the master notebooks.
- Open a terminal session and create an assignment folder: `mkdir problemsetX` (i.e. problemset9 for set 9)
- Download student's .ZIP submission- and grading .CSV worksheet files from Moodle and save them in the created folder, give them a shorter name (i.e DE_submissions_W_X.zip, DE_grades_W_X.csv).
- Enter problemsetX directory `cd problemsetX`
- Convert the master notebook to otter v1: `python3 -m otter.assign.v0.convert <path_to_directory>/assignment_wX_local.ipynb assignment_wX_local_v1.ipynb ` *
- remove from the master Notebook The `pdfs` key of `generate` to avoid  this `ValueError`:
  ```
  ValueError: The 'pdfs' key of 'generate' is no longer supported. Put any 'pdfs' configurations inside the 'generate' key itself. *

  ```
- Run `otter assign assignment_wX_local_v1.ipynb dist --v1`
<!-- - Rename autograder.zip file `mv dist/autograder/*autograder*.zip dist/autograder/autograder/zip` * -->
- Make sure that the docker container is running `service docker restart`


- **run `otter_preprocessing -z ZIP_NAME -d DOWNLOAD_PATH -p TARGET_PATH -t TEMPLATE_NAME -grade`**

## Arguments


* `-z`
 Name of the downloaded student's .ZIP submission file
* `-d`
 Path to the folder containing the student's .ZIP submission file
* `-p`
 Path to the folder containing master notebook & "dist" folder
* `-t`
 Name of the downloaded moodle grading template - assumed to be in `DOWNLOAD_PATH`
* `-grade`
  If the flag is set the "otter grade" command is executed.

. * *could be automated in `otter_preprocessing.py`*
